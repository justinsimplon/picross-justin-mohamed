const SQUARE_WIDTH = brickWidth;
const REAL_WIDTH = SQUARE_WIDTH + brickPadding;

function getContext() {
    var canvas = document.getElementById("myCanvas");

    // on lui defini un context 2d en l'occurence
    var ctx = canvas.getContext("2d");

    return ctx;
}

function initElement() {
    var p = document.getElementById("myCanvas");
    // NOTE: showAlert(); ou showAlert(param); NE fonctionne PAS ici.
    // Il faut fournir une valeur de type function (nom de fonction déclaré ailleurs ou declaration en ligne de fonction).
    p.onclick = mousseclic;
    p.oncontextmenu = rightclic;
};

function mousseclic(event)

{
    let x = event.offsetX - brickOffsetLeft;
    console.log("x=" + x);
    let y = event.offsetY - brickOffsetTop;
    // alert("y="+y);
    var p = document.getElementById("myCanvas");
    //1:  X / taille du carré
    x = x / REAL_WIDTH;
    y = y / REAL_WIDTH;
    // 2 : math.floor

    x = Math.floor(x);
    y = Math.floor(y);
    console.log("case : " + x);

    // test si x < 0 ou x > nb de case

    if (x < 0 || y < 0) {
        return;
    } else if (x > 4 || y > 4) {
        return;
    } else {
        tabVerif[y][x] = 1; //dans tabloVerif remplace 0 par 1 quand clic gauche

        // 3:  y / taille du carré

        // fillRect( x * REAL_WIDTH, y * REAL_WIDTH, SQUARE_WIDTH, SQUARE_WIDTH)
        setcolor(brickOffsetLeft + x * REAL_WIDTH, brickOffsetTop + y * REAL_WIDTH, "black");
    }

}

function rightclic(event)

{
    let x = event.offsetX - brickOffsetLeft;
    // alert("x="+x);
    let y = event.offsetY - brickOffsetTop;
    // alert("y="+y);

    // 1:  X / taille du carré
    x = x / REAL_WIDTH;
    y = y / REAL_WIDTH;
    // 2 : math.floor

    x = Math.floor(x);
    y = Math.floor(y);

    if (x < 0 || y < 0) {
        return;
    } else if (x > 4 || y > 4) {
        return;
    } else {
        tabVerif[y][x] = 0;

        setcolor(brickOffsetLeft + x * (REAL_WIDTH), brickOffsetTop + y * REAL_WIDTH, "white");
    }
}

function setcolor(x, y, color) {
    ctx = getContext();
    ctx.fillStyle = color;
    ctx.fillRect(x, y, SQUARE_WIDTH, SQUARE_WIDTH);

}


function loadEvents() {
    initElement();
}