// nonograms
const Octopus_Height = 10;
const Octopus_Width = 10;
const Octopus_Columncount = 49;
const Octopus_Rowcount = 59;
const Superman_Width = 20;
const Superman_Height = 20;
const Superman_Rowcount = 14;
const Superman_Columncount = 10;
const Titanic_Width = 60;
const Titanic_Height = 60;
const Titanic_Rowcount = 5;
const Titanic_Columncount = 5;

// global constants
const brickRowCount = Titanic_Rowcount;
const brickColumnCount = Titanic_Columncount;
let brickWidth = Titanic_Width;
let brickHeight = Titanic_Height;
const brickPadding = 1;
const brickOffsetTop = 50;
const brickOffsetLeft = 100;

function draw(){
//on recupére l'élement mycanvas

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext('2d');
var x = canvas.width/2;
var y = canvas.height-30;
var dx = 2;
var dy = -2;
 

// Definition of picross games

function NumeroHorizontal_Titanic(){
    ctx.fillStyle = "black";
    ctx.font = '48px serif';
    ctx.fillText('1', 40, 100);
    ctx.fillText('2', 40, 160);
    ctx.fillText('2', 40, 220);
    ctx.fillText('3', 40, 280);
    ctx.fillText('2', 40, 340);
}

function NumeroVertical_Titanic(){
    ctx.fillStyle = "black";
    ctx.font = '48px serif';
    ctx.fillText('0', 110, 40 );
    ctx.fillText('1', 170, 40 );
    ctx.fillText('5', 230, 40 );
    ctx.fillText('4', 300, 40 );
    ctx.fillText('0', 360, 40 );
}
function NumeroVertical_Superman(){
    ctx.fillStyle = "black";
    ctx.font = '18px serif';
    ctx.fillText('8', 105, 40 );
    ctx.fillText('3', 125, 40 );
    ctx.fillText('3', 145, 40 );
    ctx.fillText('2', 165, 40 );
    ctx.fillText('4', 185, 40 );
    ctx.fillText('2', 210, 40 );
    ctx.fillText('2', 230, 40 );
    ctx.fillText('7', 250, 40 );
    ctx.fillText('3', 270, 40 );
    ctx.fillText('4', 290, 40 );
}
function NumeroHorizontal_Superman(){
    ctx.fillStyle = "black";
    ctx.font = '18px serif';
    ctx.fillText('1', 40, 100);
    ctx.fillText('2', 40, 160);
    ctx.fillText('2', 40, 220);
    ctx.fillText('3', 40, 280);
    ctx.fillText('2', 40, 340);
}
function NumeroVertical_Octopus(){
    ctx.fillStyle = "black";
    ctx.font = '10px serif';
    ctx.fillText('0', 110, 40 );
    ctx.fillText('1', 170, 40 );
    ctx.fillText('5', 230, 40 );
    ctx.fillText('4', 300, 40 );
    ctx.fillText('0', 360, 40 );
}
function NumeroHorizontal_Octopus(){
    ctx.fillStyle = "black";
    ctx.font = '10px serif';
    ctx.fillText('1', 40, 100);
    ctx.fillText('2', 40, 160);
    ctx.fillText('2', 40, 220);
    ctx.fillText('3', 40, 280);
    ctx.fillText('2', 40, 340);
}

var bricks = [];

// brick array initialization
for(var c = 0; c < brickColumnCount; c++) {
    bricks[c] = [];
    for(var r = 0; r < brickRowCount; r++) {
        bricks[c][r] = { x:0, y:0};
    }
}

function drawBricks() {
    for(var c = 0; c < brickColumnCount; c++) {
        for(var r = 0; r < brickRowCount; r++){
            var brickX = (c*(brickWidth+brickPadding)) + brickOffsetLeft;
            var brickY = (r*(brickHeight+brickPadding)) + brickOffsetTop;
            
            bricks[c][r].x = brickX;
            bricks[c][r].y = brickY;
            
            Cadrillage();
        }
        function Cadrillage () {
            ctx.beginPath();
            ctx.rect(brickX, brickY, brickWidth, brickHeight);
            ctx.fillStyle = "white";
            ctx.fill();
            ctx.strokeStyle = "black";
            ctx.stroke();
        };
    }

    NumeroHorizontal_Titanic ();
    NumeroVertical_Titanic ();
}
drawBricks();
loadEvents();
}